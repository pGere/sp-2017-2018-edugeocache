/* global angular */
angular.module('edugeocache', []);

var jeStevilo = function(vrednost) {
  return !isNaN(parseFloat(vrednost)) && isFinite(vrednost);
};

var formatirajRazdaljo = function() {
  return function(razdalja) {
    var vrednostRazdalje, enota;
    if (typeof razdalja != undefined && jeStevilo(razdalja)) {
      if (razdalja > 1) {
        vrednostRazdalje = parseFloat(razdalja).toFixed(1);
        enota = 'km';
      } else {
        vrednostRazdalje = parseInt(razdalja * 1000, 10);
        enota = 'm';
      }
      return vrednostRazdalje + enota;
    } else {
      return "?";
    }
  };
};

var edugeocachePodatki = function($http) {
  var koordinateTrenutneLokacije = function(lat, lng) {
    return $http.get('/api/lokacije?lng=' + lng + '&lat=' + lat + '&maxRazdalja=100');
  };
  return {
    koordinateTrenutneLokacije: koordinateTrenutneLokacije
  };
};

var geolokacija = function() {
  /* global navigator */
  var vrniLokacijo = function(pkUspesno, pkNapaka, pkNiLokacije) {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(pkUspesno, pkNapaka);
    } else {
      pkNiLokacije();
    }
  };
  return {
    vrniLokacijo: vrniLokacijo
  };
};

var seznamLokacijCtrl = function($scope, edugeocachePodatki, geolokacija) {
  $scope.sporocilo = "Pridobivam trenutno lokacijo.";
  
  $scope.pridobiPodatke = function(lokacija) {
    var lat = lokacija.coords.latitude,
        lng = lokacija.coords.longitude;
    $scope.sporocilo = "Iščem bližnje lokacije.";
    edugeocachePodatki.koordinateTrenutneLokacije(lat, lng).then(
      function success(odgovor) {
        $scope.sporocilo = odgovor.data.length > 0 ? "" : "Ne najdem lokacij.";
        $scope.data = { lokacije: odgovor.data };
      }, 
      function error(odgovor) {
        $scope.sporocilo = "Prišlo je do napake!";
        console.log(odgovor.e);
      });
  };
  
  $scope.prikaziNapako = function(napaka) {
    $scope.$apply(function() {
      $scope.sporocilo = napaka.message;
    });
  };

  $scope.niLokacije = function() {
    $scope.$apply(function() {
      $scope.sporocilo = "Vaš brskalnik ne podpira geolociranja!";
    });
  };
  
  geolokacija.vrniLokacijo($scope.pridobiPodatke, $scope.prikaziNapako, $scope.niLokacije);
};

var prikaziOceno = function() {
  return {
    scope: {
      trenutnaOcena: '=ocena'
    },
    templateUrl: "/angular/ocena-zvezdice.html"
  };
};

angular
  .module('edugeocache')
  .controller('seznamLokacijCtrl', seznamLokacijCtrl)
  .filter('formatirajRazdaljo', formatirajRazdaljo)
  .directive('prikaziOceno', prikaziOceno)
  .service('edugeocachePodatki', edugeocachePodatki)
  .service('geolokacija', geolokacija);