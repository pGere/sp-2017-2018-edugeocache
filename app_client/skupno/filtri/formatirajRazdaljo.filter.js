(function() {
  /* global angular */
  
  var jeStevilo = function(vrednost) {
    return !isNaN(parseFloat(vrednost)) && isFinite(vrednost);
  };
  
  var formatirajRazdaljo = function() {
    return function(razdalja) {
      var vrednostRazdalje, enota;
      if (typeof razdalja != undefined && jeStevilo(razdalja)) {
        if (razdalja > 1) {
          vrednostRazdalje = parseFloat(razdalja).toFixed(1);
          enota = 'km';
        } else {
          vrednostRazdalje = parseInt(razdalja * 1000, 10);
          enota = 'm';
        }
        return vrednostRazdalje + enota;
      } else {
        return "?";
      }
    };
  };
  
  angular
    .module('edugeocache')
    .filter('formatirajRazdaljo', formatirajRazdaljo);
})();